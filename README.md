**Test-automation-wase-quickstart**

Project for test automation, covering functionality testing. 

---

## Tools

Maven
Cucumber-JVM
JUnit
Selenium Webdriver

1. Maven
2. Cucumber-JVM
3. JUnit
4. Selenium Webdriver

---

## Requirements

In order to utilise this project you need to have the following installed locally:

1. Apache Maven 3
2. Chrome 77 ((UI tests use Chrome by default, still cannot be changed in config) <-- ToDO)
3. Chromedriver 77
4. Java 1.8

---

## Usage

In order to build and run the project navigate to waesAutomationProject directory and run:

**mvn clean install**

To run all scenarios, navigate to waesAutomationProject directory and run:

**mnv test**

You can use tags to run specifics failures, example: 

**mvn test "-Dcucumber.options= --tags @Login"**

Valid tags: @Login, @Profile, @SignUp

---

## Reporting

Reports will be generated after every run.

Report in HTML format will be located under: target/cucumber-html-report
Report in JSON format will be located under: target/cucumber-json-report.json

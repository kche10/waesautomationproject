@Regression
@Details
Feature: Details Section
  Description: This feature will test what is displayed in the Details page

  @Ignore
  Scenario Outline: Details page displays user information

    Given User is on Home Page
    When User navigate to Login Page
    And User enters '<userName>' and '<password>'
    Then User navigate to details page
    Then User information is displayed '<expectedInformation>'

    Examples:
      | userName | password | expectedMessage                                                                                 |
      | admin    | hero     | Name: Zuper Dooper Dev Email address: zd.dev@wearewaes.com                                      |
      | dev      | wizard   | How are you doing, Zuper Dooper Dev? Your super power: Debug a repellent factory storage.       |
      | tester   | maniac   | How are you doing, Al Skept-Cal Tester? Your super power: Voltage AND Current.                  |


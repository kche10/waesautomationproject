@Regression
@Profile
Feature: Profile Section
  Description: This feature will test what is displayed in the profile page

  @Test
  Scenario Outline: Profile page displays correct message and information for admin user

    Given User is on Home Page
    When User navigate to Login Page
    And User enters '<userName>' and '<password>'
    Then User should see the profile page with title: 'Your Profile'
    And The welcome message displayed is: '<expectedMessage>'
    Examples:
      | userName | password | expectedMessage                         |
      | admin    | hero     | How are you doing, Amazing Admin?       |
      | dev      | wizard   | How are you doing, Zuper Dooper Dev?    |
      | tester   | maniac   | How are you doing, Al Skept-Cal Tester? |


  @Ignore
  Scenario: Profile table displays all name and emails

    Given User is on Home Page
    When User navigate to Login Page
    And User enters '<userName>' and '<password>'
    Then Table with correct names and emails values displays






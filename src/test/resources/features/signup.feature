@Regression
@SignUp
Feature: Signup Action
  Description: This feature will test the Sign up functionality

  @Test
  Scenario: Create a new account

    Given User is on Home Page
    When User navigate to SignUp page
    And User enters the following data in the required fields
      |test|pass|John|John@mail.com|3|April|1990|
    Then User should see the Login Successfully message: 'Logged in as John (John@mail.com)! log out'

  @Ignore
  Scenario: Create a new account with missing data (This should be performed for all the required fields)

    Given User is on Home Page
    When User navigate to SignUp page
    And User enters all the required fields but userName
    Then User should see the validation message: 'Please fill out this field.'

  @Ignore
  Scenario: Create a new account with incorrect email format

    Given User is on Home Page
    When User navigate to SignUp page
    And User enters an incorrect email format
    Then User should see the validation message: 'Please include an '@' in the email address. <wrongEmail> is missing an '@'.'

  @Ignore
  Scenario: Create a new account with invalid DOB

    Given User is on Home Page
    When User navigate to SignUp page
    And User enters all the required fields with and invalid DOB (Feb-30)
    Then User should see the validation message: 'Invalid DOB' <--- This validation is not performed.

  @Ignore
  Scenario: Create a new account with incorrect DOB

    Given User is on Home Page
    When User navigate to SignUp page
    And User enters all the required fields with and invalid DOB (Jun-31)
    Then User should see the validation message: 'Invalid DOB' <--- This validation is not performed.

  @Ignore
  Scenario: Create an account with an existing username

    Given User is on Home Page
    When User navigate to SignUp page
    And User enters all the required fields with and existing username
    Then User should see the validation message: 'Invalid DOB' <--- This validation is not performed.

  @Ignore
  Scenario: Create an account with an existing email

    Given User is on Home Page
    When User navigate to SignUp page
    And User enters all the required fields with and existing email
    Then User should see the validation message: 'Invalid DOB' <--- This validation is not performed.

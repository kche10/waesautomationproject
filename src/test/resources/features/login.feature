@Regression
@Login
Feature: Login Action
  Description: This feature will test a LogIn and LogOut functionality

  @Test
  Scenario Outline: Login with valid Credentials

    Given User is on Home Page
    When User navigate to Login Page
    And User enters '<userName>' and '<password>'
    Then User should see the Login Successfully message: '<expectedMessage>'

    Examples:
      | userName | password | expectedMessage                                                     |
      | admin    | hero     | Logged in as Amazing Admin (a.admin@wearewaes.com)! log out         |
      | dev      | wizard   | Logged in as Zuper Dooper Dev (zd.dev@wearewaes.com)! log out       |
      | tester   | maniac   | Logged in as Al Skept-Cal Tester (as.tester@wearewaes.com)! log out |
      | test     | test     | Wrong credentials. You can do it, try again!                        |

  @Test
  Scenario: Logout after login

    Given User is on Home Page
    When User navigate to Login Page
    And User enters 'admin' and 'hero'
    Then User can press Logout link
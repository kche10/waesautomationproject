package heroes.Pages;

import common.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginPage extends BasePage {

    @FindBy( id = "username_input")
    public WebElement userNameInputField;

    @FindBy (id = "password_input")
    public WebElement passwordInputField;

    @FindBy (id = "login_button")
    public WebElement loginButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void setUserNameInputField (String userName) {
        wait.until(ExpectedConditions.visibilityOf(userNameInputField));
        System.out.println("Sending Keys \"" + userName + "\" To \"UserName  Field\"");
        userNameInputField.sendKeys(userName);
    }

    public void setPasswordInputField (String password) {
        wait.until(ExpectedConditions.visibilityOf(passwordInputField));
        System.out.println("Sending Keys \"" + password + "\" To \"UserName  Field\"");
        passwordInputField.sendKeys(password);
    }

    public void clickLoginButton() {
        wait.until(ExpectedConditions.elementToBeClickable(loginButton));
        System.out.println("Clicking Login button");
        loginButton.click();
    }


}

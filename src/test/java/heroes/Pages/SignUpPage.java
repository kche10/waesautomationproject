package heroes.Pages;


import common.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class SignUpPage extends BasePage {

    public SignUpPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "username_input")
    public WebElement userNameInputField;

    @FindBy(id = "password_input")
    public WebElement passwordInputField;

    @FindBy(id = "name_input")
    public WebElement nameInputField;

    @FindBy(id = "email_input")
    public WebElement emailInputField;

    @FindBy(id = "submit_button")
    public WebElement signUpButton;

    @FindBy(id = "day_select")
    public WebElement daySelectDropdown;

    @FindBy(id = "month_select")
    public WebElement monthSelectDropdown;

    @FindBy (id = "year_select")
    public WebElement yearSelectDropdown;

    public void setUserNameInputField (String userName) {
        wait.until(ExpectedConditions.visibilityOf(userNameInputField));
        System.out.println("Sending Keys \"" + userName + "\" To \"UserName  Field\"");
        userNameInputField.sendKeys(userName);
    }

    public void setPasswordInputField (String password) {
        wait.until(ExpectedConditions.visibilityOf(passwordInputField));
        System.out.println("Sending Keys \"" + password + "\" To \"UserName  Field\"");
        passwordInputField.sendKeys(password);
    }

    public void setNameInputField (String name) {
        wait.until(ExpectedConditions.visibilityOf(nameInputField));
        System.out.println("Sending Keys \"" + name + "\" To \"Name  Field\"");
        nameInputField.sendKeys(name);
    }

    public void setEmailInputField (String email) {
        wait.until(ExpectedConditions.visibilityOf(emailInputField));
        System.out.println("Sending Keys \"" + email + "\" To \"Name  Field\"");
        emailInputField.sendKeys(email);
    }

    public void setMonthSelectDropdown (String month) {
        Select select = new Select(monthSelectDropdown);
        select.selectByVisibleText(month);
    }

    public void setDaySelectDropdown (String day) {
        Select select = new Select(daySelectDropdown);
        select.selectByVisibleText(day);
    }

    public void setYearSelectDropdown (String year) {
        Select select = new Select(yearSelectDropdown);
        select.selectByVisibleText(year);
    }

    public void clickSignUpButton() {
        wait.until(ExpectedConditions.elementToBeClickable(signUpButton));
        System.out.println("Clicking Sign Up button");
        signUpButton.click();
    }
}

package heroes.Pages;

import common.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProfilePage extends BasePage {

    @FindBy( xpath = "//p[@class='status-module--status__text--3FhKN']")
    public WebElement statusMessage;

    public ProfilePage(WebDriver driver) {
        super(driver);

    }

    @FindBy(xpath = "//*[@id=\"___gatsby\"]//section/p[1]")
    public WebElement welcomeMessage;

    @FindBy(xpath = "//*[@id=\"users_list_table\"]/tbody/tr[1]/th[1]")
    public WebElement tableTitleName;

    @FindBy(xpath = "//*[@id=\"users_list_table\"]/tbody/tr[1]/th[2]")
    public WebElement tableTitleEmail;

    @FindBy(xpath = "//*[@id=\"users_list_table\"]/tbody/tr[2]/td[1]")
    public WebElement valueNameRow1;

    @FindBy(xpath = "//*[@id=\"users_list_table\"]/tbody/tr[2]/td[2]")
    public WebElement valueEmailRow1;

    @FindBy(xpath = "//*[@id=\"users_list_table\"]/tbody/tr[3]/td[1]")
    public WebElement valueNameRow2;

    @FindBy(xpath = "//*[@id=\"users_list_table\"]/tbody/tr[3]/td[2]")
    public WebElement valueEmailRow2;

    @FindBy(xpath = "//*[@id=\"users_list_table\"]/tbody/tr[4]/td[1]")
    public WebElement valueNameRow3;

    @FindBy(xpath = "//*[@id=\"users_list_table\"]/tbody/tr[4]/td[2]")
    public WebElement valueEmailRow3;

    public String getSectionTitle(){
        return sectionTitle.getText();
    }

    public String getTableTitleName(){
        return tableTitleName.getText();
    }

    public String getTableTitleEmail(){
        return tableTitleEmail.getText();
    }

    public String getValueNameRow1(){
        return valueNameRow1.getText();
    }

    public String getValueEmailRow1(){
        return valueEmailRow1.getText();
    }

    public String getValueNameRow2(){
        return valueNameRow2.getText();
    }

    public String getValueEmailRow2(){
        return valueEmailRow2.getText();
    }

    public String getValueNameRow3(){
        return valueNameRow3.getText();
    }

    public String getValueEmailRow3(){
        return valueEmailRow3.getText();
    }

    public String getWelcomeMessage(){
        return welcomeMessage.getText();
    }
}

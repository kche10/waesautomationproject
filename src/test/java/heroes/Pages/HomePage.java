package heroes.Pages;

import common.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class HomePage extends BasePage {

    @FindBy( id = "login_link")
    public WebElement loginLink;

    @FindBy(id = "signup_link")
    public WebElement signupLink;

    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
    public void launchPage (String url){
        this.driver.get(url);
    }

    public HomePage launchPage() {
        driver.navigate().to("https://waesworks.bitbucket.io/\"");
        return PageFactory.initElements(driver, HomePage.class);
    }
    public void clickLoginPage(){
        wait.until(ExpectedConditions.elementToBeClickable(loginLink));
        System.out.println("Clicking Login button");
        loginLink.click();
    }

    public void clickSignUpPage(){
        wait.until(ExpectedConditions.elementToBeClickable(signupLink));
        System.out.println("Clicking SignUp button");
        signupLink.click();
    }

}

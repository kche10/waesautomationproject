package heroes;


import common.pages.BasePage;
import common.pages.TestBase;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import heroes.Pages.HomePage;
import heroes.Pages.LoginPage;
import heroes.Pages.ProfilePage;
import heroes.Pages.SignUpPage;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StepDefinitions extends TestBase {
    private HomePage homePage = new HomePage(driver);
    private LoginPage loginPage = new LoginPage(driver);
    private ProfilePage profilePage = new ProfilePage(driver);
    private SignUpPage signUpPage = new SignUpPage(driver);
    private BasePage basePage = new BasePage(driver);


    //-------GIVEN----------
    @Given("^User is on Home Page$")
    public void goToHomePage(){
        homePage.launchPage("https://waesworks.bitbucket.io/");
    }

    //-------WHEN----------
    @When("^User navigate to Login Page$")
    public void clickLoginPage () {
        homePage.clickLoginPage();
    }

    @When("User navigate to SignUp page$")
    public void clickSignUpPage () { homePage.clickSignUpPage();}

    //-------AND----------
    @And("User enters '(.*)' and '(.*)'$")
    public void  login (String userName, String password){
        loginPage.setUserNameInputField(userName);
        loginPage.setPasswordInputField(password);
        loginPage.clickLoginButton();
    }

    @And("^User enters the following data in the required fields$")
    public void signUp (DataTable userData){
        List<String> list = userData.asList(String.class);
        signUpPage.setUserNameInputField(list.get(0));
        signUpPage.setPasswordInputField(list.get(1));
        signUpPage.setNameInputField(list.get(2));
        signUpPage.setEmailInputField(list.get(3));
        signUpPage.setDaySelectDropdown(list.get(4));
        signUpPage.setMonthSelectDropdown(list.get(5));
        signUpPage.setYearSelectDropdown(list.get(6));
        signUpPage.clickSignUpButton();
    }

    @And("^The welcome message displayed is: '(.*)'$")
    public void verifyWelcomeMessage (String expectedWelcomeMessage){
        assertTrue("Expected: "+ expectedWelcomeMessage + "current: " + profilePage.getWelcomeMessage(),profilePage.getWelcomeMessage().equalsIgnoreCase(expectedWelcomeMessage));
        System.out.println("Checking expected message");
    }

    //----THEN----
    @Then("^User should see the profile page with title: '(.*)'$")
    public void verifySectionTitle (String expectedSectionTitle){
        assertTrue("Expected: "+ expectedSectionTitle + "current: " + basePage.getSectionTitle(),basePage.getSectionTitle().equalsIgnoreCase(expectedSectionTitle));
        System.out.println("Expected title verified");
    }

    @Then("^User should see the Login Successfully message: '(.*)'$")
    public void verifyStatusMessage (String expectedStatusMessage){
        assertTrue("Expected: "+ expectedStatusMessage + "current: " + basePage.getStatusMessage(),basePage.getStatusMessage().equalsIgnoreCase(expectedStatusMessage));
        System.out.println("Expected message verified");
    }

    @Then("^User can press Logout link$")
    public void clickLogOutButton() {
        basePage.clickLogOutButton();
    }


    //ToDO: Move to TestBase


    @Before
    public void before(Scenario scenario) {
        System.out.println("------------------------------");
        System.out.println("Starting - " + scenario.getName());
        System.out.println("------------------------------");
    }

    @After
    public void cleanUp(){
        driver.close();
    }

}

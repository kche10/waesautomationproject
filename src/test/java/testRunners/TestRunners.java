package testRunners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@CucumberOptions(
        features = "src/test/resources/features",
        glue = {""},
        tags = {"not @Ignore","@Regression"},
        monochrome = true,plugin = {"html:target/cucumber-html-report", "json:target/cucumber-json-report.json" }
        )

@RunWith(Cucumber.class)
public class TestRunners {
}



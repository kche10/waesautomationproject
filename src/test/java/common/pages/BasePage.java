package common.pages;

import heroes.Pages.HomePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    private static final int TIMEOUT = 5;
    private static final int POLLING = 100;

    protected WebDriver driver;
    protected WebDriverWait wait;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, TIMEOUT, POLLING);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, TIMEOUT), this);
    }

    @FindBy(xpath = "//section[@class='view-module--view--3wzVy']/h1")
    public WebElement sectionTitle;

    @FindBy (xpath = "//p[@class='status-module--status__text--3FhKN']")
    public WebElement statusMessage;

    @FindBy (xpath = "//*[@id=\"status\"]/p/a")
    public WebElement logoutButton;

    public String getSectionTitle(){
        return sectionTitle.getText();
    }

    public String getStatusMessage(){
        return statusMessage.getText();
    }

    public void clickLogOutButton() {
        wait.until(ExpectedConditions.elementToBeClickable(logoutButton));
        System.out.println("Clicking Logout button");
        logoutButton.click();
    }

}

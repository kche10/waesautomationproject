package common.pages;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestBase {
    public WebDriver driver;
    public WebDriverWait wait;

    public TestBase() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
    }
    public void launchPage (String url){
        this.driver.get(url);
    }
}
